/* AWT */
import java.awt.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/* Swing (for windows) */
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/* 
 * Exp4j
 * Utile pour résoudre une expression de manière mathématique
 * N'utilise pas de fonction eval ou similaire (aucun risque d'injection de code)
 */
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * Main class
 * @author Baptiste CHARLIER
 */
public class calculator
{
	/* Instance of UI class */
    private ui ui;
    
    /**
     * Constructor
     */
    public calculator()
    {
    	ui = new ui();
    }
    
    /**
     * Resolve given expression, does NOT handle exceptions
     */
    private Double resolve(String expression)
    {
    	//Get display value
    	String exp_str = ui.get_display();
    		
    	//Generate the expression
    	ExpressionBuilder exp_builder = new ExpressionBuilder(exp_str);
    		
    	Expression exp = exp_builder.build();
    	
    	//Evaluate our expression
    	return exp.evaluate();
    }
    
    /**
     * UI class
     * Manages window, buttons and display
     */
    private class ui extends JFrame
    {
    	private final String[] 	tab_string 			= new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "=", "C", "M+", "M-", "MR", "MC", "*", "/", "+", "-", "^", "(", ")", "cos", "sin", "sqrt", "bin" };
        private JPanel 			container 			= new JPanel();
        private JButton[] 		tab_button 			= new JButton[tab_string.length];
    	private Font 			font 				= new Font("Arial", Font.PLAIN, 20);
    	private JLabel 			display 			= new JLabel("0");
    	private JPanel 			panel_operators 	= new JPanel();
    	private JPanel 			panel_numbers 		= new JPanel();
    	private JPanel 			panel_display 		= new JPanel();
    	private JPanel 			panel_bottom		= new JPanel();
    	private double			memory				= 0;
    	
    	/**
    	 * Constructor (initializes window and register listeners)
    	 */
    	public ui()
    	{
    		/* Init window */
    		init_window();
    		
            /* start::display */
    		display.setFont(font);
            display.setHorizontalAlignment(JLabel.RIGHT);
            display.setPreferredSize(new Dimension(440, 20));
            
            panel_display.add(display);
            panel_display.setBorder(BorderFactory.createLineBorder(Color.black));
            panel_display.setPreferredSize(new Dimension(440, 30));
            /* end::display */
            
            panel_bottom.setPreferredSize(new Dimension(440, 250));
            //panel_bottom.setBorder(BorderFactory.createLineBorder(Color.black));
            
            panel_numbers.setPreferredSize(new Dimension(210, 290));
            panel_operators.setPreferredSize(new Dimension(210, 290));
            
            for (int i = 0; i < tab_string.length; i++)
            {
                tab_button[i] = new JButton(tab_string[i]);
                tab_button[i].setPreferredSize(new Dimension(65, 40));
                tab_button[i].addActionListener(new button_listener());
                
                switch(tab_string[i])
                {
	                case "C":
	                case "*":
	                case "/":
	                case "+":
	                case "-":
	                case "cos":
	                case "sin":
	                case "sqrt":
	                case "bin":
	                case "^":
	                case "M+":
	                case "M-":
	                case "MR":
	                case "MC":
	                {
	                	panel_operators.add(tab_button[i]);
	                    break;
	                }
	                default: // 0123456789.=
	                {
	                	panel_numbers.add(tab_button[i]);
	                	panel_numbers.add(tab_button[i]);
	                	break;
	                }
                }
            }
            
            panel_bottom.setLayout(new FlowLayout());
            panel_bottom.add(panel_numbers);
            panel_bottom.add(panel_operators);
            
            container.add(panel_display, BorderLayout.NORTH);
            container.add(panel_bottom, BorderLayout.CENTER);
            
            this.setContentPane(container);  
    	}
    	
    	/**
    	 * Sets window data (called by constructor)
    	 */
    	private void init_window()
    	{
            this.setSize(450, 340); 								//size
            this.setTitle("Jalulcator"); 							//title
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 	//close operation
            this.setLocationRelativeTo(null); 						//spawn centered
            this.setResizable(false); 								//disallow resizing (fixed size)
            this.setVisible(true); 									//make window visible
    	}
    	
    	/**
    	 * Append a new string to the display
    	 * Takes care of default display value and error (0 and ERR)
    	 * @param String str String to append
    	 */
    	public void append_display(String str)
    	{
    		if (display.getText() == "0" || display.getText() == "ERR")
    			display.setText("");

    		set_display(display.getText() + str);
    	}
    	
    	/**
    	 * Gets display value
    	 * @return String display value
    	 */
    	public String get_display()
    	{
    		return display.getText();
    	}
    	
    	/**
    	 * Set display value
    	 * @param String str Value to set
    	 */
    	public void set_display(String str)
    	{
    		display.setText(str);
    	}
    	
    	/**
    	 * Clear display
    	 */
    	public void clear_display()
    	{
    		display.setText("0");
    	}
    	
    	/**
    	 * Display current memory value
    	 */
    	public void display_memory()
    	{
    		set_display(Double.toString(memory));
    	}
    	
    	/**
    	 * Subtract value to memory
    	 * @param value
    	 */
    	public void subtract_memory(double value)
    	{
    		memory -= value;
    	}
    	
    	/**
    	 * Add value to memory
    	 * @param value
    	 */
    	public void add_memory(double value)
    	{
    		memory += value;
    	}
    	
    	/**
    	 * Clear memory (set to 0)
    	 */
    	public void clear_memory()
    	{
    		memory = 0;
    	}
    }
    
    /**
     * Button listener class
     * Manages listening of all buttons
     */
    private class button_listener implements ActionListener
    {
    	/**
    	 * Method called for each button pressed
    	 * @param ActionEvent e Contains data about the pressed element
    	 */
        public void actionPerformed(ActionEvent e)
        {        	
        	switch(e.getActionCommand())
        	{
    	    	case "C":
    	    	{
    	    		//User pressed 'C'
    	    		ui.clear_display();
    	    		break;
    	    	}
    	    	case "bin":
    	    	{
    	    		//User pressed 'bin'
    	    		
    	        	try
    	        	{
    	        		//Get display value
    	        		String exp = ui.get_display();
    	        		
    	        		//Evaluate the expression
    	        		double result = resolve(exp);
    	        		
    	        		//Convert double to string and display result
    	        		ui.set_display(Integer.toBinaryString((int)result));
    	        	}
    	        	catch (Exception ex)
    	        	{
    	        		//An error occured (dividing by zero, etc)
    	        		ui.set_display("ERR");
    	        	}
    	    		
    	    		//ui.set_display(ui.get_display());
    	    		break;
    	    	}
    	    	case "=":
    	    	{
    	    		//User pressed '='
    	    		
    	        	try
    	        	{
    	        		//Get display value
    	        		String exp = ui.get_display();
    	        		
    	        		//Evaluate our expression
    	        		double result = resolve(exp);
    	        		
    	        		//Convert double to string and display result
    	        		ui.set_display(Double.toString(result));
    	        	}
    	        	catch (Exception ex)
    	        	{
    	        		//An error occured (dividing by zero, etc)
    	        		ui.set_display("ERR");
    	        	}
    	    		
    	    		break;
    	    	}
    	    	case "M+":
    	    	{
    	    		//add current number displayed to memory
    	    		
    	    		try
    	    		{
    	    			//resolve expression
    	    			double result = resolve(ui.get_display());
    	    			
    	    			//add result to memory
    	    			ui.add_memory(result);
    	    			
    	    			//debug
    	    			System.out.println("Added " + result + " to memory (new value: " + ui.memory + ")");
    	    		}
    	    		catch (Exception ex)
    	    		{
    	    			//expression couldn't be resolved (dividing by zero, syntax error, etc)
    	    			ui.set_display("ERR");
    	    		}
    	    		
    	    		break;
    	    	}
    	    	case "M-":
    	    	{
    	    		//Subtract current number displayed with memory
    	    		
    	    		try
    	    		{
    	    			//resolve expression
    	    			double result = resolve(ui.get_display());
    	    			
    	    			//add result to memory
    	    			ui.subtract_memory(result);
    	    			
    	    			//debug
    	    			System.out.println("Subtracted " + result + " from memory (new value: " + ui.memory + ")");
    	    		}
    	    		catch (Exception ex)
    	    		{
    	    			//expression couldn't be resolved (dividing by zero, syntax error, etc)
    	    			ui.set_display("ERR");
    	    		}
    	    		
    	    		break;
    	    	}
    	    	case "MR":
    	    	{
    	    		//display memory
    	    		ui.display_memory();
    	    		
    	    		//debug
    	    		System.out.println("Memory restored (value: " + ui.memory + ")");
    	    		
    	    		break;
    	    	}
    	    	case "MC":
    	    	{
    	    		//clear memory (set to 0)
    	    		ui.clear_memory();
    	    		
    	    		//debug
    	    		System.out.println("Memory cleared");
    	    		
    	    		break;
    	    	}
    	    	default:
    	    	{
    	    		//User pressed one of the other buttons 0123456789-+/*() etc
    	    		ui.append_display(e.getActionCommand());
    	    		break;
    	    	}
        	}        	
        }
    }
}













